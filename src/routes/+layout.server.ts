import config from "$lib/server/config"

import type { LayoutServerLoad } from "./$types"

export const load: LayoutServerLoad = () => {
  return {
    appTitle: config.title,
    baseUrl: config.baseUrl,
    matomo: config.matomo,
    simulateurSocioFiscalUrl: config.simulateurSocioFiscalUrl,
    territoiresUrl: config.territoiresUrl,
  }
}

export const prerender = true
