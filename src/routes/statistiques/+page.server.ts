import config from "$lib/server/config"

import type { PageServerLoad } from "./$types"

interface MatomoPageVisit {
  label?: string
  nb_visits: number
  nb_hits: number
  url?: string
  segment?: string
  subtable?: MatomoPageVisit[]
}

interface MatomoEvent {
  nb_events: number
  segment: string
}

interface StatItem {
  visites?: MatomoPageVisit
  autresVisites?: {
    [label: string]: MatomoPageVisit
  }
  utilisations?: MatomoPageVisit[]
}

async function getPageVisitsData(
  date: string,
): Promise<MatomoPageVisit[] | undefined> {
  const pageVisitsQuery: URLSearchParams = new URLSearchParams()
  pageVisitsQuery.append("module", "API")
  pageVisitsQuery.append("method", "Actions.getPageUrls")
  pageVisitsQuery.append("idSite", "1")
  pageVisitsQuery.append("period", "year")
  pageVisitsQuery.append("date", date)
  pageVisitsQuery.append("expanded", "1")
  pageVisitsQuery.append("depth", "2")
  pageVisitsQuery.append("format", "JSON")
  pageVisitsQuery.append("token_auth", config.matomoToken!)
  const pageVisitsUrl = new URL(
    `?${pageVisitsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const pageVisitsRes = await fetch(pageVisitsUrl, {
    method: "POST",
  })
  if (!pageVisitsRes.ok) {
    console.error(
      `Error ${
        pageVisitsRes.status
      } while creating a share link at ${pageVisitsUrl}\n\n${await pageVisitsRes.text()}`,
    )
    return
  }
  return (await pageVisitsRes.json()) as MatomoPageVisit[]
}

async function getSimulatorData(
  date: string,
): Promise<MatomoPageVisit[] | undefined> {
  const simulatorPageVisitsQuery: URLSearchParams = new URLSearchParams()
  simulatorPageVisitsQuery.append("module", "API")
  simulatorPageVisitsQuery.append("method", "Actions.getPageUrls")
  simulatorPageVisitsQuery.append("idSite", "8")
  simulatorPageVisitsQuery.append("period", "year")
  simulatorPageVisitsQuery.append("date", date)
  simulatorPageVisitsQuery.append("format", "JSON")
  simulatorPageVisitsQuery.append("token_auth", config.matomoToken!)
  const simulatorPageVisitsUrl = new URL(
    `?${simulatorPageVisitsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const simulatorPageVisitsRes = await fetch(simulatorPageVisitsUrl, {
    method: "POST",
  })
  if (!simulatorPageVisitsRes.ok) {
    console.error(
      `Error ${
        simulatorPageVisitsRes.status
      } while creating a share link at ${simulatorPageVisitsUrl}\n\n${await simulatorPageVisitsRes.text()}`,
    )
    return
  }
  return (await simulatorPageVisitsRes.json()) as MatomoPageVisit[]
}

async function getPageEventsData(
  date: string,
): Promise<MatomoEvent[] | undefined> {
  const pageEventsQuery: URLSearchParams = new URLSearchParams()
  pageEventsQuery.append("module", "API")
  pageEventsQuery.append("method", "Events.getName")
  pageEventsQuery.append("idSite", "1")
  pageEventsQuery.append("period", "year")
  pageEventsQuery.append("date", date)
  pageEventsQuery.append("format", "JSON")
  pageEventsQuery.append("token_auth", config.matomoToken!)
  const pageEventsUrl = new URL(
    `?${pageEventsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const pageEventsRes = await fetch(pageEventsUrl, {
    method: "POST",
  })
  if (!pageEventsRes.ok) {
    console.error(
      `Error ${
        pageEventsRes.status
      } while creating a share link at ${pageEventsUrl}\n\n${await pageEventsRes.text()}`,
    )
    return
  }
  return (await pageEventsRes.json()) as MatomoEvent[]
}

async function getDataCircoData(
  date: string,
): Promise<
  [
    MatomoPageVisit | undefined,
    MatomoPageVisit | undefined,
    MatomoPageVisit[] | undefined,
  ]
> {
  const dataCircoDownloadsQuery: URLSearchParams = new URLSearchParams()
  dataCircoDownloadsQuery.append("module", "API")
  dataCircoDownloadsQuery.append("method", "Actions.getDownloads")
  dataCircoDownloadsQuery.append("idSite", "5")
  dataCircoDownloadsQuery.append("period", "year")
  dataCircoDownloadsQuery.append("date", date)
  dataCircoDownloadsQuery.append("format", "JSON")
  dataCircoDownloadsQuery.append(
    "segment",
    "downloadUrl=$.pdf;downloadUrl!@.png;downloadUrl!@.json",
  )
  dataCircoDownloadsQuery.append("token_auth", config.matomoToken!)
  const dataCircoDownloadsUrl = new URL(
    `?${dataCircoDownloadsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const dataCircoDownloadsRes = await fetch(dataCircoDownloadsUrl, {
    method: "POST",
  })
  let dataCircoDownloads: MatomoPageVisit | undefined = undefined
  if (!dataCircoDownloadsRes.ok) {
    console.error(
      `Error ${
        dataCircoDownloadsRes.status
      } while creating a share link at ${dataCircoDownloadsUrl}\n\n${await dataCircoDownloadsRes.text()}`,
    )
  } else {
    dataCircoDownloads = (
      await dataCircoDownloadsRes.json()
    )[0] as MatomoPageVisit
  }

  // Téléchargements de PDf sur le nouveau DataCirco
  const dataCircoNewDownloadsQuery: URLSearchParams = new URLSearchParams()
  dataCircoNewDownloadsQuery.append("module", "API")
  dataCircoNewDownloadsQuery.append("method", "Actions.getDownloads")
  dataCircoNewDownloadsQuery.append("idSite", "10")
  dataCircoNewDownloadsQuery.append("period", "year")
  dataCircoNewDownloadsQuery.append("date", date)
  dataCircoNewDownloadsQuery.append("format", "JSON")
  dataCircoNewDownloadsQuery.append(
    "segment",
    "downloadUrl=$.pdf;downloadUrl!@.png;downloadUrl!@.json",
  )
  dataCircoNewDownloadsQuery.append("token_auth", config.matomoToken!)
  const dataCircoNewDownloadsUrl = new URL(
    `?${dataCircoNewDownloadsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const dataCircoNewDownloadsRes = await fetch(dataCircoNewDownloadsUrl, {
    method: "POST",
  })
  let dataCircoNewDownloads: MatomoPageVisit | undefined = undefined
  if (!dataCircoNewDownloadsRes.ok) {
    console.error(
      `Error ${
        dataCircoNewDownloadsRes.status
      } while creating a share link at ${dataCircoNewDownloadsUrl}\n\n${await dataCircoDownloadsRes.text()}`,
    )
  } else {
    dataCircoNewDownloads = (
      await dataCircoNewDownloadsRes.json()
    )[0] as MatomoPageVisit
  }

  // Visites sur le nouveau DataCirco
  const dataCircoNewPageVisitsQuery: URLSearchParams = new URLSearchParams()
  dataCircoNewPageVisitsQuery.append("module", "API")
  dataCircoNewPageVisitsQuery.append("method", "Actions.getPageUrls")
  dataCircoNewPageVisitsQuery.append("idSite", "10")
  dataCircoNewPageVisitsQuery.append("period", "year")
  dataCircoNewPageVisitsQuery.append("date", date)
  dataCircoNewPageVisitsQuery.append("format", "JSON")
  dataCircoNewPageVisitsQuery.append("token_auth", config.matomoToken!)
  const dataCircoNewPageVisitsUrl = new URL(
    `?${dataCircoNewPageVisitsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const dataCircoNewPageVisitsRes = await fetch(dataCircoNewPageVisitsUrl, {
    method: "POST",
  })
  let dataCircoNewPageVisitsResJson: MatomoPageVisit[] | undefined
  if (!dataCircoNewPageVisitsRes.ok) {
    console.error(
      `Error ${
        dataCircoNewPageVisitsRes.status
      } while creating a share link at ${dataCircoNewPageVisitsUrl}\n\n${await dataCircoNewPageVisitsRes.text()}`,
    )
  } else {
    dataCircoNewPageVisitsResJson =
      (await dataCircoNewPageVisitsRes.json()) as MatomoPageVisit[]
  }

  return [
    dataCircoDownloads,
    dataCircoNewDownloads,
    dataCircoNewPageVisitsResJson,
  ]
}

async function getBarometreData(
  date: string,
): Promise<[MatomoPageVisit[] | undefined, MatomoPageVisit | undefined]> {
  // Visites sur le Baromètre
  const barometreVisitsQuery: URLSearchParams = new URLSearchParams()
  barometreVisitsQuery.append("module", "API")
  barometreVisitsQuery.append("method", "Actions.getPageUrls")
  barometreVisitsQuery.append("idSite", "6")
  barometreVisitsQuery.append("period", "year")
  barometreVisitsQuery.append("date", date)
  barometreVisitsQuery.append("expanded", "1")
  barometreVisitsQuery.append("depth", "1")
  barometreVisitsQuery.append("format", "JSON")
  barometreVisitsQuery.append("token_auth", config.matomoToken!)
  const barometreVisitsUrl = new URL(
    `?${barometreVisitsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const barometreVisitsRes = await fetch(barometreVisitsUrl, {
    method: "POST",
  })
  let barometreVisitsResJson: MatomoPageVisit[] | undefined = undefined
  if (!barometreVisitsRes.ok) {
    console.error(
      `Error ${
        barometreVisitsRes.status
      } while creating a share link at ${barometreVisitsUrl}\n\n${await barometreVisitsRes.text()}`,
    )
  } else {
    barometreVisitsResJson =
      (await barometreVisitsRes.json()) as MatomoPageVisit[]
  }

  // Téléchargements des données open data du Baromètre
  const barometreOpenDataDownloadsQuery: URLSearchParams = new URLSearchParams()
  barometreOpenDataDownloadsQuery.append("module", "API")
  barometreOpenDataDownloadsQuery.append("method", "Actions.getDownloads")
  barometreOpenDataDownloadsQuery.append("idSite", "6")
  barometreOpenDataDownloadsQuery.append("period", "year")
  barometreOpenDataDownloadsQuery.append("date", date)
  barometreOpenDataDownloadsQuery.append("format", "JSON")
  barometreOpenDataDownloadsQuery.append(
    "segment",
    "downloadUrl=$.xlsx,downloadUrl=$.csv",
  )
  barometreOpenDataDownloadsQuery.append("token_auth", config.matomoToken!)
  const barometreOpenDataDownloadsUrl = new URL(
    `?${barometreOpenDataDownloadsQuery}`,
    "https://matomo.leximpact.dev/",
  ).toString()
  const barometreOpenDataDownloadsRes = await fetch(
    barometreOpenDataDownloadsUrl,
    {
      method: "POST",
    },
  )
  let barometreOpenDataDownloads: MatomoPageVisit | undefined = undefined
  if (!barometreOpenDataDownloadsRes.ok) {
    console.error(
      `Error ${
        barometreOpenDataDownloadsRes.status
      } while creating a share link at ${barometreOpenDataDownloadsUrl}\n\n${await barometreOpenDataDownloadsRes.text()}`,
    )
  } else {
    barometreOpenDataDownloads = (
      await barometreOpenDataDownloadsRes.json()
    )[0] as MatomoPageVisit
  }

  return [barometreVisitsResJson, barometreOpenDataDownloads]
}

export const load: PageServerLoad = async () => {
  if (config.matomoToken === undefined) {
    return undefined
  }

  const baseYear = 2023
  const currentYear = new Date().getFullYear()
  const matomoStats: { [year: string]: { [name: string]: StatItem } } = {}

  for (let i = 0; i < currentYear - baseYear + 1; i++) {
    const year = (baseYear + i).toString()
    const date = `${year}-01-01`

    const pageVisitsResJson = await getPageVisitsData(date)

    const simulatorPageVisitsResJson = await getSimulatorData(date)

    const pageEventsResJson = await getPageEventsData(date)
    const dotationsNbEstimations = pageEventsResJson?.find(
      (event) =>
        event.segment === "eventName==simule+avec+communes+types+et+strates",
    )

    const [oldDataCircoDownloads, dataCircoDownloads, dataCircoVisitsResJson] =
      await getDataCircoData(date)

    const [barometreVisitsResJson, barometreOpenDataDownloads] =
      await getBarometreData(date)

    const portail: StatItem = {}
    const simulateur: StatItem = {}
    const barometre: StatItem = {}
    const dataCirco: StatItem = {}
    const dotations: StatItem = {}
    const memos: StatItem = {}
    for (const item of pageVisitsResJson ?? []) {
      if (item.label?.includes("dotations")) {
        dotations.visites = {
          nb_visits: (dotations.visites?.nb_visits ?? 0) + item.nb_visits,
          nb_hits: (dotations.visites?.nb_hits ?? 0) + item.nb_hits,
        }
        dotations.utilisations = [
          {
            nb_visits: dotationsNbEstimations?.nb_events ?? 0,
            nb_hits: dotationsNbEstimations?.nb_events ?? 0,
          },
        ]
        continue
      }

      if (item.label === "leximpact_prepare_data") {
        const memoVisits = item.subtable?.find(
          ({ label }) => label === "memos",
        )?.subtable
        if (memoVisits !== undefined) {
          memos.utilisations = memoVisits
            .filter(
              ({ label }) =>
                !label?.includes("memos.html") &&
                !label?.includes("publications.html") &&
                !label?.includes("memo_style.html") &&
                !label?.includes("memo_agregats_pote.html"),
            )
            .sort((a, b) =>
              a.nb_visits < b.nb_visits
                ? 1
                : a.nb_visits === b.nb_visits
                  ? 0
                  : -1,
            )
        }
        memos.visites = item
        continue
      }

      if (item.url?.includes("https://socio-fiscal.leximpact.an.fr/")) {
        simulateur.visites = {
          nb_visits: (simulateur.visites?.nb_visits ?? 0) + item.nb_visits,
          nb_hits: (simulateur.visites?.nb_hits ?? 0) + item.nb_hits,
        }
        continue
      }

      if (
        item.url?.includes("https://leximpact.an.fr/") &&
        !item.url?.includes("ir")
      ) {
        portail.visites = {
          nb_visits: (portail.visites?.nb_visits ?? 0) + item.nb_visits,
          nb_hits: (portail.visites?.nb_hits ?? 0) + item.nb_hits,
        }
        continue
      }
    }

    const oldSimulatorUsage = pageVisitsResJson?.find((item) =>
      item.label?.includes("parametric_reform"),
    )
    const simulatorUsage = simulatorPageVisitsResJson?.find((item) =>
      item.label?.includes("parametric_reform"),
    )
    simulateur.utilisations = [
      {
        nb_visits:
          (simulateur.utilisations?.nb_visits ?? 0) +
          (oldSimulatorUsage?.nb_visits ?? 0) +
          (simulatorUsage?.nb_visits ?? 0),
        nb_hits:
          (simulateur.utilisations?.nb_hits ?? 0) +
          (oldSimulatorUsage?.nb_hits ?? 0) +
          (simulatorUsage?.nb_hits ?? 0),
      },
    ]

    // Stats Baromètre

    const barometreVisits = barometreVisitsResJson?.find(
      ({ label }) => label === "/embedded",
    )

    barometre.visites = {
      nb_visits: barometreVisits?.nb_visits,
      nb_hits: barometreVisits?.nb_hits,
    }

    const barometreOtherVisits = barometreVisitsResJson?.find(
      ({ label }) => label === "embedded",
    )

    const barometreDossiersLegislatifsVisits = Object.fromEntries(
      barometreOtherVisits?.subtable
        ?.filter(
          ({ label }) =>
            label === "dossiers" ||
            label === "/fonctionnement" ||
            label === "/statistiques",
        )
        .map((item) => [item.label?.match(/[a-z]+/)?.[0], item]) ?? [],
    )
    barometre.autresVisites = {
      ...barometreDossiersLegislatifsVisits,
      downloads: barometreOpenDataDownloads,
    }

    // DataCirco
    const oldDataCircoVisits = pageVisitsResJson?.find(
      ({ label }) => label === "/datacirco",
    )
    const dataCircoVisits = dataCircoVisitsResJson?.find(
      ({ label }) => label === "/index",
    )

    dataCirco.visites = {
      nb_visits:
        (oldDataCircoVisits?.nb_visits ?? 0) +
        (dataCircoVisits?.nb_visits ?? 0),
      nb_hits:
        (oldDataCircoVisits?.nb_hits ?? 0) + (dataCircoVisits?.nb_hits ?? 0),
    }

    dataCirco.utilisations = [
      {
        nb_visits:
          (oldDataCircoDownloads?.nb_visits ?? 0) +
          (dataCircoDownloads?.nb_visits ?? 0),
        nb_hits:
          (oldDataCircoDownloads?.nb_hits ?? 0) +
          (dataCircoDownloads?.nb_hits ?? 0),
      },
    ]

    matomoStats[year] = {
      portail,
      simulateur,
      barometre,
      dataCirco,
      dotations,
      memos,
    }
  }

  return { matomoStats }
}
