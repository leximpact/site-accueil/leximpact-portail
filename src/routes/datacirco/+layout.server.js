import { redirect } from "@sveltejs/kit"

/** @type {import('./$types').LayoutServerLoad} */
export function load({ locals }) {
  if (!locals.user) {
    redirect(301, "https://datacirco.leximpact.an.fr/")
  }
}
