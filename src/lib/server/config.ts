import "dotenv/config"

import { validateConfig } from "$lib/server/auditors/config"

export interface Config {
  allowRobots: boolean
  baseUrl: string
  matomo?: {
    prependDomain?: boolean
    siteId: number
    subdomains?: string
    url: string
  }
  matomoToken?: string
  simulateurSocioFiscalUrl: string
  territoiresUrl: string
  title: string
}

const [validConfig, error] = validateConfig({
  allowRobots: process.env["ALLOW_ROBOTS"],
  baseUrl: process.env["BASE_URL"],
  matomo:
    process.env["MATOMO_SITE_ID"] && process.env["MATOMO_URL"]
      ? {
          prependDomain: process.env["MATOMO_PREPEND_DOMAIN"],
          siteId: process.env["MATOMO_SITE_ID"],
          subdomains: process.env["MATOMO_SUBDOMAINS"],
          url: process.env["MATOMO_URL"],
        }
      : null,
  matomoToken: process.env["MATOMO_TOKEN"],
  simulateurSocioFiscalUrl: process.env["SIMULATEUR_SOCIO_FISCAL_URL"],
  territoiresUrl: process.env["TERRITOIRES_URL"],
  title: process.env["TITLE"],
})
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
const config = validConfig as Config
export default config
