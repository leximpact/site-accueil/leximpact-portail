const flowbytePlugin = require("flowbite/plugin")
const colors = require("tailwindcss/colors")

const config = {
  content: [
    "./src/**/*.{html,js,svelte,ts}",
    "./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}",
  ],
  plugins: [flowbytePlugin],
  theme: {
    extend: {
      blur: {
        xs: "1.2px",
        xxs: "0.8px",
      },
      boxShadow: {
        button: "2px 2px 8px 0 rgba(0,0,0,.25)",
        inner_title: "inset  0px 0px 16px rgba(0,0,0,.15)",
        card: "0px 0px 30px 10px rgba(0,0,0,0.15)",
      },
      colors: {
        gray: colors.neutral,
        "le-bleu": "#343bff",
        "le-bleu-light": "#d2dfff",
        "le-jaune-light": "#EEEA8A",
        "le-jaune": "#ded500",
        "le-jaune-dark": "#9d970b",
        "le-jaune-very-dark": "#6E6A08",
        "le-rouge-bill": "#ff6b6b",
        "le-gris-dispositif": "#5E709E",
        "le-gris-dispositif-ultralight": "#EBEFFA",
        "le-gris-dispositif-light": "#CCD3E7",
        "le-gris-dispositif-dark": "#2F406A",
        "le-vert-validation": "#13CC03",
        "le-vert-validation-dark": "#377330",
        "le-vert": {
          50: "#f1f0e6",
          100: "#e2e1cd",
          200: "#c5c39c",
          300: "#a8a66a",
          400: "#8b8839",
          500: "#6e6a07",
          600: "#635f06",
          700: "#585506",
          800: "#424004",
          900: "#2c2a03",
          950: "#161501",
        },
      },
    },
    fontFamily: {
      sans: ["Lato", "sans-serif"],
      serif: ["Lora", "serif"],
    },
    screens: {
      sm: "640px",
      // => @media (min-width: 640px) { ... }
      md: "768px",
      // => @media (min-width: 768px) { ... }
      lg: "1024px",
      // => @media (min-width: 1024px) { ... }
      xl: "1280px",
      // => @media (min-width: 1280px) { ... }
      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
      "3xl": "2200px",
      // => @media (min-width: 2200px) { ... }
    },
  },
}

module.exports = config
