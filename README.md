# Portail LexImpact

Le source du site https://leximpact.an.fr

## Installation

Le code source de ce site peut être téléchargé en local avec la commande suivante :

```bash
git clone https://git.leximpact.dev/leximpact/site-accueil
```

Puis, ce code requiert [Node.js](https://nodejs.org) dans une version identique à celle définie dans le fichier [.gitlab-ci.yml](./.gitlab-ci.yml).

Enfin, sa configuration et son installation peuvent être réalisées avec les commandes suivantes :

```bash
cd leximpact-portail/
ln -s example.env .env # Ou créer un nouveau fichier .env à partir de example.env.
npm install
```

> Pour mettre à jour les librairies exécuter :
> `npm update` pour les mises à jour mineures
> puis `npm install monPaquetPrefere@latest` pour les mises à jour majeures à vérifier manuellement.

## Utilisation en mode développement

```bash
npm run dev
```

## Génération du site statique

```bash
npm run build
```

## Mise en production des modifications

Pour mettre en production les modifications, rendez-vous sur le projet [leximpact-portail-deploy](https://git.leximpact.dev/leximpact/site-accueil/leximpact-portail-deploy) et consulter le Read.me.

## Composants UI

### Icônes

Utiliser la librairie : https://remixicon.com/

Si le choix et trop restreint, toutes les icônes de https://iconify.design/ peuvent être utilisées.

```html
<iconify-icon
    class="" #utiliser les classes tailwind
    icon="ri-nom-icone" #mettre le nom de l'icone.
/>
```

**Si l'icône doit être alignée avec un texte**, utiliser la class Tailwind `align-[-0.2rem]` pour ajuster l'emplacement de l'icône par rapport au texte. La taille d'une icone dans un `<span>` s'ajuste avec les class de textes, par ex. `text-lg` :

```html
<span>
  <iconify-icon class="align-[-0.2rem] text-base" icon="ri-nom-icone" />
  Mon texte
</span>
```

### Liens/boutons

#### Boutons-outils

**Usage :** Boutons utilisés pour accéder à des fonctionnalités de l'interface.
<br>**Caractéristiques :** Discrétion. Bouton de petite taille, avec hover discret.

![image.png](./image.png)

Selon la situation l'icone peut être à droite ou à gauche. Une margin de 1 `ml-1` ou `mr-1` est toujours mise entre le texte et l'icone.
La taille de l'icône est normalement de `text-lg` mais pourra être ajustée selon le résultat. Il faudra alors modifier l'alignement avec `align-[-0.2rem]`

De type bouton :

```html
<button class="text-sm uppercase text-gray-600 hover:text-black" on:click="{}">
  Changer de dispositif
  <iconify-icon class="ml-1 align-[-0.2rem] text-lg" icon="ri-arrow-up-line" />
</button>
```

De type lien :

```html
<a
  class="text-sm uppercase text-gray-600 hover:text-black"
  data-sveltekit-noscroll
  href=""
>
  Changer de dispositif
  <iconify-icon class="ml-1 align-[-0.2rem] text-lg" icon="ri-arrow-up-line" />
</a>
```

**Options :**

- Modifier l'emplacement du bouton, utiliser `place-self-end` / `place-selff-start`. Cette class fonctionne si le bouton est dans un `<div class="flex">` et permet d'éviter de recréer un `<div>`.
